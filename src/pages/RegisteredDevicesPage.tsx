import * as countries from 'i18n-iso-countries'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { InferType } from 'yup'

import { DataTable } from '../components/DataTable'
import { Layout } from '../components/Layout'
import { parseDateString } from '../helpers/dateHelpers'
import { objectSchema, stringSchema } from '../helpers/SchemaHelpers'

countries.registerLocale(require('i18n-iso-countries/langs/en.json'))

type DeviceDTO = InferType<typeof registeredDeviceSchema>
const registeredDeviceSchema = objectSchema({
  country_code: stringSchema(),
  user_email: stringSchema(),
  user_id: stringSchema(),
  optic_number: stringSchema(),
  store_name: stringSchema(),
  branch_name: stringSchema(),
  devicetype_code: stringSchema(),
  devicetype_id: stringSchema(),
  purchase_date: stringSchema(),
})

export default function RegisteredDevicesPage() {
  const { t } = useTranslation()

  return (
    <Layout title={t('registered_devices.title')}>
      <DataTable<DeviceDTO>
        dataIndex="optic_number"
        resource_url="/registered-devices/"
        columns={[
          { label: t('registered_devices.device_id'), key: 'optic_number' },
          { label: t('registered_devices.device_code'), key: 'devicetype_code' },
          {
            label: t('registered_devices.purchase_date'),
            content: ({ purchase_date }) => parseDateString(purchase_date, ''),
          },
          {
            label: t('registered_devices.country'),
            content: ({ country_code }) => countries.getName(country_code, 'en'),
          },
          { label: t('registered_devices.store'), key: 'store_name' },
          // { label: 'User', content: ({ user_email, user_id }) => `${user_id} ${user_email}` },
        ]}
        toolbar={{
          search: true,
          filter_by_date: true,
          pagination: true,
          export: true,
        }}
      />
    </Layout>
  )
}
