import {
  Box,
  Dialog,
  Divider,
  Grid,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TextField,
  Typography,
} from '@material-ui/core'
import { Close } from '@material-ui/icons'
import { Skeleton } from '@material-ui/lab'
import { DateRangeDelimiter, DesktopDateRangePicker } from '@material-ui/pickers'
import { useRequest } from 'ahooks'
import { format } from 'date-fns'
import { startOfMonth } from 'date-fns/esm'
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, Route, useParams } from 'react-router-dom'
import { InferType } from 'yup'

import { useAPI, useHttpResource } from '../api/API'
import { DataTable } from '../components/DataTable'
import { Layout } from '../components/Layout'
import { parseDateString } from '../helpers/dateHelpers'
import { numberSchema, objectSchema, stringSchema } from '../helpers/SchemaHelpers'
import { CompanyInfo } from './BrandDetails'

// type AnalysisHistoryDetails = InferType<typeof analysisHistoryDetailsSchema>
// const analysisHistoryDetailsSchema = objectSchema({
//   birth: stringSchema(),
//   country: stringSchema(),
//   email: stringSchema(),
//   ethnicity: stringSchema(),
//   gender: numberSchema(),
//   id: numberSchema(),
//   name: stringSchema(),
//   phone: stringSchema(),
//   skin_color: stringSchema(),
//   surname: stringSchema(),
// })

type Statistics = InferType<typeof statisticsSchema>
const statisticsSchema = objectSchema({
  total_customers: stringSchema(),
  total_analysis: stringSchema(),
  total_registered_devices: stringSchema(),
})

type StoreDetailsStatistics = InferType<typeof storeDetailsSchema>
const storeDetailsSchema = objectSchema({
  month: stringSchema(),
  number_of_analyses: numberSchema(),
  number_of_counselors: numberSchema(),
  number_of_customers: numberSchema(),
  number_of_devices: numberSchema(),
  store_email: stringSchema(),
  store_id: numberSchema(),
  store_name: stringSchema(),
  store_phone: stringSchema(),
  total_new_analysis_this_month: numberSchema(),
  total_new_analysis_this_week: numberSchema(),
  total_new_customers_this_month: numberSchema(),
  total_new_customers_this_week: numberSchema(),
  week: objectSchema({
    from: stringSchema(),
    to: stringSchema(),
  }),
})

type AnalysisHistory = InferType<typeof analysisHistorySchema>
const analysisHistorySchema = objectSchema({
  branch_name: stringSchema(),
  number_of_analyses: numberSchema(),
  number_of_counselors: numberSchema(),
  number_of_customers: numberSchema(),
  store_id: numberSchema(),
  store_name: stringSchema(),
})

function LoadingStoreDetails() {
  const { t } = useTranslation()

  return (
    <>
      <Table size="small">
        <TableRow>
          <TableCell align="right">{t('statistics.store_id')}:</TableCell>
          <TableCell align="left">
            <Skeleton />
          </TableCell>
          <TableCell align="right">{t('statistics.total_bms_overall')}:</TableCell>
          <TableCell align="left">
            <Skeleton />
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell align="right">{t('statistics.store_name')}:</TableCell>
          <TableCell align="left">
            <Skeleton />
          </TableCell>
          <TableCell align="right">{t('statistics.total_devices_overall')}:</TableCell>
          <TableCell align="left">
            <Skeleton />
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell align="right">{t('statistics.contact_information')}:</TableCell>
          <TableCell align="left">
            <Skeleton />
          </TableCell>
          <TableCell align="right" />
          <TableCell align="left" />
        </TableRow>
      </Table>

      <Box marginBottom={2} />
      <table cellPadding={2} width="100%" style={{ border: '1px solid black' }}>
        <tr>
          <td align="right">{t('statistics.month')}:</td>
          <td align="left">
            <Skeleton />
          </td>
          <td align="right">{t('statistics.week')}:</td>
          <td align="left">
            <Skeleton />
          </td>
        </tr>
      </table>

      <Box marginBottom={2} />
      <table cellPadding={2} width="100%">
        <tr>
          <td align="left">
            <Typography>{t('statistics.total_customers')}</Typography>
            <Divider />
            {t('statistics.overall')}: <Skeleton />
          </td>
          <td align="left">
            <Typography>{t('statistics.total_analysis')}</Typography>
            <Divider />
            {t('statistics.overall')}: <Skeleton />
          </td>
        </tr>
      </table>

      <Box marginBottom={2} />
      <table cellPadding={2} width="100%">
        <tr>
          <td align="left">
            <Typography>{t('statistics.new_customers')}</Typography>
            <Divider />
            <div>
              {t('statistics.this_week')}: <Skeleton />
            </div>
            <div>
              {t('statistics.this_month')}: <Skeleton />
            </div>
          </td>
          <td align="left">
            <Typography>{t('statistics.new_analysis')}</Typography>
            <Divider />
            <div>
              {t('statistics.this_week')}: <Skeleton />
            </div>
            <div>
              {t('statistics.this_month')}: <Skeleton />
            </div>
          </td>
        </tr>
      </table>
    </>
  )
}

const today = new Date()
// eslint-disable-next-line @typescript-eslint/naming-convention
const start_of_month = startOfMonth(today)

function StoreDetails() {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { store_id } = useParams<{ store_id: string }>()
  const [dateRange1, setDateRange1] = useState<{ from: Date; to: Date }>({
    from: start_of_month,
    to: today,
  })
  const [dateRange2, setDateRange2] = useState<{ from: Date; to: Date }>({
    from: start_of_month,
    to: today,
  })

  const api = useAPI()
  const { t } = useTranslation()
  const storeDetailsStatistics = useRequest<StoreDetailsStatistics>(() =>
    api.requestResource(`/statistics/reports/${store_id}/store-details/`)
  )

  const totalCustomers = useHttpResource<{ number_of_customers: number }>(
    `/statistics/reports/${store_id}/store-details/total_customers/`,
    {
      from: format(dateRange1.from, 'yyyyMMdd'),
      to: format(dateRange1.to, 'yyyyMMdd'),
    },
    {
      pagination: false,
      shouldFetch: !!store_id,
    }
  )

  const totalAnalysis = useHttpResource<{ 'count(temp.batch_id)': number }>(
    `/statistics/reports/${store_id}/store-details/total_analysis/`,
    {
      from: format(dateRange2.from, 'yyyyMMdd'),
      to: format(dateRange2.to, 'yyyyMMdd'),
    },
    {
      pagination: false,
      shouldFetch: !!store_id,
    }
  )

  // const totalAnalysis = useRequest<{ 'count(temp.batch_id)': number }>(
  //   (from: string, to: string) =>
  //     api.requestResource(`/statistics/reports/${store_id}/store-details/total_analysis/`, {
  //       params: {
  //         from,
  //         to,
  //       },
  //     }),
  //   {
  //     defaultParams: {
  //       from: format(dateRange2.from, 'yyyyMMdd'),
  //       to: format(dateRange2.to, 'yyyyMMdd'),
  //     },
  //   }
  // )

  return (
    <Box padding={3}>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Typography variant="body1">
          <b>{t('statistics.store_details')}</b>
        </Typography>
        <Link to="/statistics">
          <IconButton>
            <Close />
          </IconButton>
        </Link>
      </Box>

      {storeDetailsStatistics.data ? (
        <>
          <Table size="small">
            <TableRow>
              <TableCell align="right">{t('statistics.store_id')}:</TableCell>
              <TableCell align="left">{storeDetailsStatistics.data.store_id}</TableCell>
              <TableCell align="right">{t('statistics.total_bms_overall')}:</TableCell>
              <TableCell align="left">{storeDetailsStatistics.data.number_of_counselors}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="right">{t('statistics.store_name')}:</TableCell>
              <TableCell align="left">{storeDetailsStatistics.data.store_name}</TableCell>
              <TableCell align="right">{t('statistics.total_devices_overall')}:</TableCell>
              <TableCell align="left">{storeDetailsStatistics.data.number_of_devices}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="right">{t('statistics.contact_information')}:</TableCell>
              <TableCell align="left">
                {storeDetailsStatistics.data.store_email} {storeDetailsStatistics.data.store_phone}
              </TableCell>
              <TableCell align="right" />
              <TableCell align="left" />
            </TableRow>
          </Table>

          <Box marginBottom={2} />
          <table
            cellPadding={6}
            width="100%"
            style={{ border: '1px solid black', textAlign: 'center', lineHeight: '24px' }}
          >
            <tr>
              <td style={{ borderBottom: '1px solid black' }}>
                {t('statistics.month')}: {storeDetailsStatistics.data.month}
              </td>
              <td style={{ borderBottom: '1px solid black' }}>
                {t('statistics.week')}: {parseDateString(storeDetailsStatistics.data.week.from)} -{' '}
                {parseDateString(storeDetailsStatistics.data.week.to)}
              </td>
            </tr>
            <tr>
              <td>
                <b>{t('statistics.total_customers')}</b>
                <Divider />
                {t('statistics.overall')}: {storeDetailsStatistics.data.number_of_customers}
                <Box marginTop={2}>
                  <b>{t('statistics.new_customers')}</b>
                  <Divider />
                  <div>
                    {t('statistics.this_week')}:{' '}
                    {storeDetailsStatistics.data.total_new_customers_this_week}
                  </div>
                  <div>
                    {t('statistics.this_month')}:{' '}
                    {storeDetailsStatistics.data.total_new_customers_this_month}
                  </div>
                </Box>
              </td>
              <td>
                <b>{t('statistics.total_analysis')}</b>
                <Divider />
                {t('statistics.overall')}: {storeDetailsStatistics.data.number_of_analyses}
                <Box marginTop={2}>
                  <b>{t('statistics.new_analysis')}</b>
                  <Divider />
                  <div>
                    {t('statistics.this_week')}:{' '}
                    {storeDetailsStatistics.data.total_new_analysis_this_week}
                  </div>
                  <div>
                    {t('statistics.this_month')}:{' '}
                    {storeDetailsStatistics.data.total_new_analysis_this_month}
                  </div>
                </Box>
              </td>
            </tr>
          </table>
        </>
      ) : (
        <LoadingStoreDetails />
      )}

      <Box marginBottom={2} />
      <table cellPadding={10} width="100%" style={{ border: '1px solid black' }}>
        <tr>
          <td align="left">
            <Typography>{t('statistics.select_date')}</Typography>
            <Divider />
            <Box paddingBottom={1} />
            <DesktopDateRangePicker
              clearable
              value={[dateRange1.from, dateRange1.to]}
              onChange={([from, to]) => {
                if (from && to) {
                  setDateRange1({ from, to })
                }
              }}
              renderInput={(startProps, endProps) => (
                <>
                  <TextField
                    size="small"
                    fullWidth
                    {...startProps}
                    variant="standard"
                    helperText=""
                  />
                  <DateRangeDelimiter> ~ </DateRangeDelimiter>
                  <TextField
                    size="small"
                    fullWidth
                    {...endProps}
                    variant="standard"
                    helperText=""
                  />
                </>
              )}
            />

            <Box paddingTop={2} />
            <Typography>
              {t('statistics.customers')}:{' '}
              {totalCustomers.data ? (
                ((totalCustomers.data as unknown) as Record<string, number>)?.number_of_customers
              ) : (
                <Skeleton />
              )}
            </Typography>
          </td>
          <td align="left">
            <Typography>{t('statistics.select_date')}</Typography>
            <Divider />
            <Box paddingBottom={1} />
            <DesktopDateRangePicker
              clearable
              value={[dateRange2.from, dateRange2.to]}
              onChange={([from, to]) => {
                if (from && to) {
                  setDateRange2({ from, to })
                }
              }}
              renderInput={(startProps, endProps) => (
                <>
                  <TextField
                    size="small"
                    fullWidth
                    {...startProps}
                    variant="standard"
                    helperText=""
                  />
                  <DateRangeDelimiter> ~ </DateRangeDelimiter>
                  <TextField
                    size="small"
                    fullWidth
                    {...endProps}
                    variant="standard"
                    helperText=""
                  />
                </>
              )}
            />

            <Box paddingTop={2} />
            <Typography>
              {t('statistics.customers')}:{' '}
              {totalAnalysis.data ? (
                ((totalAnalysis.data as unknown) as Record<string, number>)['count(temp.batch_id)']
              ) : (
                <Skeleton />
              )}
            </Typography>
          </td>
        </tr>
      </table>
    </Box>
  )
}

export function StatisticsAndReports() {
  const { t } = useTranslation()
  const api = useAPI()
  const statistics = useRequest<Statistics>(() => api.requestResource('/statistics/'))
  const { data: companyInfo } = useRequest<{ data: CompanyInfo }>(() =>
    api.requestResource('/brand-details/company_info/')
  )

  return (
    <Layout title={t('statistics.title')}>
      <Route path="/statistics/reports/:store_id/store-details/">
        {({ match }) => (
          <Dialog open={!!match} disableEnforceFocus>
            <StoreDetails />
          </Dialog>
        )}
      </Route>

      <TableContainer component={Paper} variant="outlined">
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>
                <Typography gutterBottom>{t('brand_details.company')}</Typography>
                <Typography>
                  <b>{companyInfo?.data?.name} </b>
                </Typography>
              </TableCell>
              <TableCell>
                <Typography gutterBottom>{t('brand_details.address')}</Typography>
                <Typography>
                  <b>{companyInfo?.data?.address} </b>
                </Typography>
              </TableCell>
              <TableCell>
                <Typography gutterBottom>{t('brand_details.contact_information')}</Typography>
                <Typography>
                  <b>{companyInfo?.data?.email || companyInfo?.data?.phone} </b>
                </Typography>
              </TableCell>
              <TableCell>
                <Typography gutterBottom>{t('brand_details.started_since')}</Typography>
                <Typography>
                  <b>{parseDateString(companyInfo?.data?.registration_date || '')} </b>
                </Typography>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>

      <Box paddingY={2}>
        <Paper variant="outlined">
          <Box padding={2}>
            <Grid container spacing={2}>
              <Grid item sm>
                <Typography>
                  <b>{t('statistics.total_customers')}</b>
                  <Divider />
                </Typography>
                <Typography>
                  <b>{t('statistics.overall')}:</b> {statistics.data?.total_customers}
                </Typography>
              </Grid>
              <Grid item sm>
                <Typography>
                  <b>{t('statistics.total_devices_overall')}</b>
                  <Divider />
                </Typography>
                <Typography>
                  <b>{t('statistics.overall')}:</b> {statistics.data?.total_registered_devices}
                </Typography>
              </Grid>
              <Grid item sm>
                <Typography>
                  <b>{t('statistics.total_analysis')}</b>
                  <Divider />
                </Typography>
                <Typography>
                  <b>{t('statistics.overall')}:</b> {statistics.data?.total_analysis}
                </Typography>
              </Grid>
            </Grid>
          </Box>
        </Paper>
      </Box>

      <Typography>
        <b>{t('analysis_history.analysis')}</b>
      </Typography>
      <Divider />
      <Box marginBottom={2} />

      <DataTable<AnalysisHistory>
        dataIndex="store_id"
        resource_url="/statistics/reports/"
        columns={[
          { label: t('brand_details.store'), key: 'store_name' },
          { label: t('brand_details.bm'), key: 'number_of_counselors' },
          { label: t('statistics.customers'), key: 'number_of_customers' },
          { label: t('analysis_history.analysis'), key: 'number_of_analyses' },
          {
            label: t('statistics.store_statistics'),
            content: ({ store_id }) => (
              <Link to={`/statistics/reports/${store_id}/store-details/`}>
                {t('analysis_history.view_details')}
              </Link>
            ),
          },
        ]}
        toolbar={{
          search: true,
          pagination: true,
          export: true,
        }}
      />
    </Layout>
  )
}
