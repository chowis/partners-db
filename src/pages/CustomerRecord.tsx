import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import { DataTable } from '../components/DataTable'
import { Layout } from '../components/Layout'
import { Customers } from './AssignedCustomers'

function CustomerRecord() {
  const { t } = useTranslation()
  return (
    <Layout title={t('customer_record.title')}>
      <DataTable<Customers>
        dataIndex="id"
        resource_url="/customer-record/"
        columns={[
          { label: t('customer_record.last_name'), key: 'surname' },
          { label: t('customer_record.first_name'), key: 'name' },
          { label: t('customer_record.service_name'), key: 'app_name' },
          { label: t('customer_record.email'), key: 'email' },
          { label: t('customer_record.phone_number'), key: 'phone' },
          { label: t('customer_record.country'), key: 'country' },
          {
            label: t('analysis_history.title'),
            content: ({ id }) => (
              <Link to={`/customer-record/${id}/`}>{t('analysis_history.view_details')}</Link>
            ),
          },
        ]}
        toolbar={{
          search: true,
          filter_by_date: true,
          pagination: true,
          export: true,
        }}
      />
    </Layout>
  )
}

export default CustomerRecord
