import {
  Box,
  Divider,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
} from '@material-ui/core'
import { useRequest } from 'ahooks'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { InferType } from 'yup'

import { useAPI } from '../api/API'
import { DataTable } from '../components/DataTable'
import { Layout } from '../components/Layout'
import { parseDateString } from '../helpers/dateHelpers'
import { numberSchema, objectSchema, stringSchema } from '../helpers/SchemaHelpers'

export type CompanyInfo = InferType<typeof companyInfoSchema>
const companyInfoSchema = objectSchema({
  address: stringSchema().nullable(),
  email: stringSchema().nullable(),
  name: stringSchema().nullable(),
  phone: stringSchema().nullable(),
  registration_date: stringSchema().nullable(),
})

type BrandAndStore = InferType<typeof brandAndStoreSchema>
const brandAndStoreSchema = objectSchema({
  branch: stringSchema(),
  country: stringSchema(),
  id: numberSchema(),
  is_active: numberSchema(),
  name: stringSchema(),
  store: stringSchema(),
  email: stringSchema(),
  phone: stringSchema(),
})

export default function BrandDetailsPage() {
  const api = useAPI()
  const { t } = useTranslation()
  const { data: companyInfo } = useRequest<{ data: CompanyInfo }>(() =>
    api.requestResource('/brand-details/company_info/')
  )

  return (
    <Layout title={t('sidebar.brand_details')}>
      <TableContainer component={Paper} variant="outlined">
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>
                <Typography gutterBottom>{t('brand_details.company')}</Typography>
                <Typography>
                  <b>{companyInfo?.data?.name} </b>
                </Typography>
              </TableCell>
              <TableCell>
                <Typography gutterBottom>{t('brand_details.address')}</Typography>
                <Typography>
                  <b>{companyInfo?.data?.address} </b>
                </Typography>
              </TableCell>
              <TableCell>
                <Typography gutterBottom>{t('brand_details.contact_information')}</Typography>
                <Typography>
                  <b>{companyInfo?.data?.email || companyInfo?.data?.phone} </b>
                </Typography>
              </TableCell>
              <TableCell>
                <Typography gutterBottom>{t('brand_details.started_since')}</Typography>
                <Typography>
                  <b>{parseDateString(companyInfo?.data?.registration_date || '')} </b>
                </Typography>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>

      <Box marginTop={3} />

      <Grid container direction="column" spacing={2} wrap="nowrap">
        <Grid item>
          <Typography>
            <b>{t('brand_details.brand_manager')}</b>
          </Typography>
          <Divider />
        </Grid>

        <Grid item xs>
          <DataTable<BrandAndStore>
            dataIndex="id"
            resource_url="/brand-details/store_and_bm/"
            columns={[
              { label: t('brand_details.store'), key: 'store' },
              { label: t('brand_details.country'), key: 'country' },
              { label: t('brand_details.bm'), key: 'name' },
              { label: t('brand_details.email'), key: 'email' },
              { label: t('brand_details.phone'), key: 'phone' },
              {
                label: t('brand_details.status'),
                content: ({ is_active }) =>
                  is_active ? t('brand_details.active') : t('brand_details.inactive'),
              },
              {
                label: t('brand_details.assigned_customers'),
                content: ({ id }) => (
                  <Link to={`/brand-details/${id}/`}>{t('brand_details.view_customers')}</Link>
                ),
              },
            ]}
            toolbar={{
              search: true,
              filter: true,
              pagination: true,
              export: true,
            }}
            filters={[
              { label: t('brand_details.all'), key: 'all' },
              {
                label: 'ID',
                key: 'id',
              },
              {
                label: t('brand_details.branch'),
                key: 'counselor_branch',
              },
              {
                label: t('brand_details.country'),
                key: 'counselor_country',
              },
              {
                label: t('brand_details.name'),
                key: 'counselor_name',
              },
              {
                label: t('brand_details.email'),
                key: 'counselor_email',
              },
              {
                label: t('brand_details.status'),
                key: 'is_active',
              },
            ]}
          />
        </Grid>
      </Grid>
    </Layout>
  )
}
