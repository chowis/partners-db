import {
  Button,
  Checkbox,
  Grid,
  MenuItem,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from '@material-ui/core'
import { CloudDownload, Refresh } from '@material-ui/icons'
import { DateRangeDelimiter, DesktopDateRangePicker } from '@material-ui/pickers'
import { useDebounce, useRequest } from 'ahooks'
import { format } from 'date-fns'
import fileDownload from 'js-file-download'
import { useSnackbar } from 'notistack'
import React, { ReactNode, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { DateParam, NumberParam, StringParam, useQueryParams, withDefault } from 'use-query-params'

import { useAPI, useHttpResource } from '../api/API'
import { useAppLanguage } from '../i18n/hooks'
import { AppTableFooter } from './AppTableFooter'
import { ExportToFileDialog } from './ExportToFileDialog'
import { TableWrapper } from './TableWrapper'

interface DataTableProps<T> {
  resource_url: string
  params?: Record<string, unknown>
  columns: Array<{ label: string; key?: keyof T; content?: (r: T) => ReactNode; hide?: boolean }>
  dataIndex?: keyof T
  toolbar?: {
    search?: boolean
    pagination?: boolean
    filter?: boolean
    filter_by_date?: boolean
    export?: boolean
  }
  pageSize?: number
  filters?: Array<{ key: string; label: string }>
  shouldFetch?: boolean
  export_url?: string
  disableCheckbox?: boolean
  normalize?: (resource: T[] | undefined) => T[] | undefined
}

export function DataTable<T>(props: DataTableProps<T>) {
  const { t } = useTranslation()
  const [isExportDialogOpen, setIsExportDialogOpen] = useState(false)
  const [selectedRowIds, setSelectedRowIds] = useState<string[]>([])
  const api = useAPI()

  const [query, setQuery] = useQueryParams({
    ...(props.toolbar?.search
      ? {
          search: withDefault(StringParam, ''),
        }
      : {}),
    ...(props.toolbar?.pagination
      ? {
          page: NumberParam,
          limit: NumberParam,
        }
      : {}),
    ...(props.toolbar?.filter
      ? {
          filter_by: withDefault(StringParam, 'all'),
        }
      : {}),
    ...(props.toolbar?.filter_by_date
      ? {
          from: withDefault(DateParam, null),
          to: withDefault(DateParam, null),
        }
      : {}),
  })

  const debouncedSearchValue = useDebounce(query.search, { wait: 500 })
  const [currentLanguage] = useAppLanguage()
  const { enqueueSnackbar } = useSnackbar()

  const exportRequest = useRequest(
    (filename: string) =>
      api.requestJSON('POST', props.export_url || `${props.resource_url}export/`, {
        data: {
          filename,
          file_type: 'csv',
          ...(props.toolbar?.search
            ? {
                search: debouncedSearchValue,
              }
            : {}),

          ...(props.toolbar?.pagination
            ? {
                page: query.page || 1,
                limit: query.limit || 20,
              }
            : {}),

          ...(selectedRowIds.length ? { selected_ids: selectedRowIds } : {}),

          ...(props.toolbar?.filter
            ? {
                filter_by: query.filter_by,
              }
            : {}),
          ...(props.toolbar?.filter_by_date
            ? {
                from: query.from ? format(query.from, 'yyyyMMdd') : undefined,
                to: query.to ? format(query.to, 'yyyyMMdd') : undefined,
              }
            : {}),
        },
      }),
    {
      manual: true,
      onSuccess: (data, [filename]) => {
        fileDownload((data as unknown) as Blob, `${filename}.csv`)
      },
      onError: (e) => {
        if (e) {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          const message = e.message as any

          enqueueSnackbar(message[currentLanguage], {
            variant: 'error',
            preventDuplicate: true,
            key: message[currentLanguage],
          })
        }
      },
    }
  )

  const results = useHttpResource(
    props.resource_url,
    {
      ...(props.toolbar?.search
        ? {
            search: debouncedSearchValue,
          }
        : {}),
      ...(props.toolbar?.pagination
        ? {
            page: query.page,
            limit: query.limit,
          }
        : {}),
      ...(props.toolbar?.filter
        ? {
            filter_by: query.filter_by,
          }
        : {}),
      ...(props.toolbar?.filter_by_date
        ? {
            from: query.from ? format(query.from, 'yyyyMMdd') : undefined,
            to: query.to ? format(query.to, 'yyyyMMdd') : undefined,
          }
        : {}),
      ...props.params,
    },
    {
      shouldFetch: typeof props.shouldFetch === 'boolean' ? props.shouldFetch : true,
      pagination: props.toolbar?.pagination,
    }
  )

  const total = results.data?.count || 0
  const pageSize = query.limit || props.pageSize || 20

  return (
    <>
      <ExportToFileDialog
        open={isExportDialogOpen}
        isLoading={exportRequest.loading}
        onClose={() => {
          setIsExportDialogOpen(false)
        }}
        onSave={(filename) => {
          exportRequest.run(filename)
        }}
      />

      <Grid container direction="column" spacing={2} wrap="nowrap">
        <Grid item container spacing={2} alignItems="center">
          <Grid item container sm spacing={2} alignItems="center">
            {props.toolbar?.search && (
              <Grid item md={3}>
                <TextField
                  label={t('datatable.search')}
                  variant="outlined"
                  size="small"
                  fullWidth
                  value={query.search}
                  onChange={(e) => {
                    setQuery({ search: e.target.value })
                  }}
                />
              </Grid>
            )}

            {props.toolbar?.filter_by_date && (
              <Grid item md={6}>
                <DesktopDateRangePicker
                  clearable
                  startText={t('date_picker.start')}
                  endText={t('date_picker.end')}
                  value={[query.from, query.to]}
                  onChange={([from, to]) => {
                    setQuery({ from, to })
                  }}
                  renderInput={(startProps, endProps) => (
                    <>
                      <TextField size="small" fullWidth {...startProps} helperText="" />
                      <DateRangeDelimiter> {t('date_picker.to')} </DateRangeDelimiter>
                      <TextField size="small" fullWidth {...endProps} helperText="" />
                    </>
                  )}
                />
              </Grid>
            )}

            {props.toolbar?.filter && (
              <Grid item md={3}>
                <TextField
                  label={t('datatable.filter_by')}
                  select
                  variant="outlined"
                  size="small"
                  fullWidth
                  value={query.filter_by}
                  onChange={(e) => {
                    setQuery({ filter_by: e.target.value })
                  }}
                >
                  {props.filters?.map(({ label, key }) => (
                    <MenuItem key={key} value={key}>
                      {label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
            )}

            {(props.toolbar?.filter || props.toolbar?.filter_by_date || props.toolbar?.search) && (
              <Grid item md={3}>
                <Button
                  variant="text"
                  onClick={() => {
                    setQuery({
                      ...(props.toolbar?.search
                        ? {
                            search: '',
                          }
                        : {}),
                      ...(props.toolbar?.filter
                        ? {
                            filter_by: 'all',
                          }
                        : {}),
                      ...(props.toolbar?.filter_by_date
                        ? {
                            from: null,
                            to: null,
                          }
                        : {}),
                    })
                  }}
                >
                  {' '}
                  {t('datatable.reset')}
                </Button>
              </Grid>
            )}
          </Grid>

          {props.toolbar?.export && (
            <Grid item>
              <Grid container spacing={2}>
                <Grid item>
                  <Button
                    onClick={() => {
                      results.revalidate()
                    }}
                    disabled={results.isValidating}
                    variant="outlined"
                    startIcon={<Refresh />}
                  >
                    {t('datatable.refresh')}
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    variant="outlined"
                    disabled={results.isValidating}
                    startIcon={<CloudDownload />}
                    onClick={() => {
                      setIsExportDialogOpen(true)
                    }}
                  >
                    {t('datatable.export')}
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>

        <Grid item xs>
          <TableWrapper
            style={{ maxHeight: '100%', overflowX: 'initial', position: 'relative' }}
            isLoading={results.isValidating}
          >
            <Table size="small" aria-label="a dense table">
              <TableHead>
                <TableRow>
                  {!props.disableCheckbox && (
                    <TableCell>
                      <Checkbox
                        color="primary"
                        indeterminate={
                          selectedRowIds?.length > 0 &&
                          selectedRowIds?.length !== results.data?.data?.length
                        }
                        checked={
                          selectedRowIds?.length === results.data?.data?.length &&
                          selectedRowIds?.length > 0
                        }
                        onChange={(_, checked) => {
                          if (checked) {
                            setSelectedRowIds(
                              results.data?.data?.map(
                                (r) => r[props.dataIndex as string] as string
                              ) || []
                            )
                          } else {
                            setSelectedRowIds([])
                          }
                        }}
                      />
                    </TableCell>
                  )}
                  {props.columns.map(
                    ({ label, key, hide }) =>
                      !hide && <TableCell key={key as string}>{label}</TableCell>
                  )}
                </TableRow>
              </TableHead>

              <TableBody>
                {results.data?.count === 0 && (
                  <TableRow>
                    <TableCell colSpan={12} size="medium">
                      <Typography align="center" color="textSecondary">
                        {t('no_data')}
                      </Typography>
                    </TableCell>
                  </TableRow>
                )}

                {!results.data ? (
                  <TableRow>
                    <TableCell colSpan={12} size="medium">
                      <Typography align="center" color="textSecondary">
                        {t('loading')}
                      </Typography>
                    </TableCell>
                  </TableRow>
                ) : (
                  (props.normalize
                    ? (props.normalize(results.data?.data as T[]) as Array<Record<string, unknown>>)
                    : results.data?.data
                  )?.map((record, idx) => (
                    // eslint-disable-next-line react/no-array-index-key
                    <TableRow key={idx}>
                      {!props.disableCheckbox && (
                        <TableCell>
                          <Checkbox
                            color="primary"
                            checked={selectedRowIds?.includes(
                              record[props.dataIndex as string] as string
                            )}
                            onChange={(_, checked) => {
                              if (checked) {
                                setSelectedRowIds([
                                  ...(selectedRowIds || []),
                                  record[props.dataIndex as string] as string,
                                ])
                              } else {
                                setSelectedRowIds(
                                  selectedRowIds?.filter(
                                    (r) => r !== record[props.dataIndex as string]
                                  )
                                )
                              }
                            }}
                          />
                        </TableCell>
                      )}
                      {props.columns.map(
                        ({ key, content, hide }, index) =>
                          !hide && (
                            <TableCell key={(key as string) || index}>
                              {key ? (record[key as string] as ReactNode) : content?.(record as T)}
                            </TableCell>
                          )
                      )}
                    </TableRow>
                  ))
                )}
              </TableBody>

              {props.toolbar?.pagination && (
                <AppTableFooter
                  total={total}
                  page={query.page || 1}
                  pageSize={pageSize}
                  colSpan={props.columns.length + 2}
                  onChangePage={(page) => {
                    setQuery({ page })
                  }}
                  onChangeRowsPerPage={(rowsPerPage) => {
                    setQuery({ limit: rowsPerPage, page: 1 })
                  }}
                />
              )}
            </Table>
          </TableWrapper>
        </Grid>
      </Grid>
    </>
  )
}
