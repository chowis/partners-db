import {
  Box,
  BoxProps,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  Menu,
  MenuItem,
  TextField,
} from '@material-ui/core'
import { Person } from '@material-ui/icons'
import { Alert } from '@material-ui/lab'
import { useRequest } from 'ahooks'
import { useSnackbar } from 'notistack'
import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

import { useAPI } from '../api/API'
import { useUserState } from '../data/UserState'
import { objectSchema, stringSchema } from '../helpers/SchemaHelpers'

type UpdatePasswordRequest = Yup.InferType<typeof updatePasswordSchema>
const updatePasswordSchema = objectSchema({
  password: stringSchema(),
  new_password: stringSchema(),
  confirm_new_password: stringSchema(),
})

interface UpdatePasswordProps {
  open: boolean
  onClose: () => void
}

export function UpdatePassword({ open, onClose }: UpdatePasswordProps) {
  const user = useUserState()
  const [error, setError] = useState()
  const form = useForm<UpdatePasswordRequest>({
    defaultValues: {
      password: '',
      new_password: '',
      confirm_new_password: '',
    },
  })
  const { t } = useTranslation()
  const { enqueueSnackbar } = useSnackbar()
  const api = useAPI()

  const onSubmit = useRequest(
    (values: UpdatePasswordRequest) =>
      api.requestJSON('POST', '/auth/change-password/', {
        data: { password: values.password, new_password: values.new_password, chowis: user.chowis },
      }),
    {
      manual: true,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      onSuccess: (e: any) => {
        if (e) {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          const message = e.message as any
          enqueueSnackbar(message[user.language], {
            variant: 'success',
            key: message[user.language],
          })
          onClose()
        }
      },
      onError: (e) => {
        if (e) {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          const message = e.message as any
          setError(message[user.language])
        }
      },
    }
  )

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>{t('update_password.title')}</DialogTitle>
      <DialogContent>
        <form onSubmit={form.handleSubmit(onSubmit.run)}>
          {!!error && (
            <Box paddingBottom={2}>
              <Alert severity="error">{error}</Alert>
            </Box>
          )}
          <Box width="300px">
            <Grid container direction="column" spacing={2}>
              <Grid item>
                <TextField
                  name="password"
                  inputRef={form.register({ required: t('update_password.required') })}
                  label={t('update_password.old_password')}
                  variant="outlined"
                  size="small"
                  fullWidth
                  error={!!form.errors.password?.message}
                  helperText={form.errors.password?.message}
                  type="password"
                />
              </Grid>
              <Grid item>
                <TextField
                  name="new_password"
                  inputRef={form.register({ required: t('update_password.required') })}
                  label={t('update_password.new_password')}
                  variant="outlined"
                  size="small"
                  fullWidth
                  error={!!form.errors.new_password?.message}
                  helperText={form.errors.new_password?.message}
                  type="password"
                />
              </Grid>
              <Grid item>
                <TextField
                  name="confirm_new_password"
                  inputRef={form.register({
                    validate: () =>
                      form.watch('new_password') === form.watch('confirm_new_password')
                        ? undefined
                        : t('update_password.no_match'),
                  })}
                  label={t('update_password.confirm_new_password')}
                  variant="outlined"
                  size="small"
                  fullWidth
                  error={!!form.errors.confirm_new_password?.message}
                  helperText={form.errors.confirm_new_password?.message}
                  type="password"
                />
              </Grid>
              <Grid item>
                <Button
                  type="submit"
                  disableElevation
                  variant="contained"
                  color="primary"
                  fullWidth
                  disabled={form.formState.isSubmitting || onSubmit.loading}
                >
                  {t('update_password.change_password')}
                </Button>
              </Grid>
            </Grid>
          </Box>
        </form>
        <Box marginTop={2} />
      </DialogContent>
    </Dialog>
  )
}

export function Header(props: BoxProps) {
  const user = useUserState()
  const { t } = useTranslation()
  const [showUpdatePasswordDialog, setShowUpdatePasswordDialog] = useState(false)
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <Box margin={2} {...props} textAlign="right">
      {showUpdatePasswordDialog && (
        <UpdatePassword
          open
          onClose={() => {
            setShowUpdatePasswordDialog(false)
          }}
        />
      )}

      <Button
        variant="outlined"
        size="large"
        aria-controls="user-menu"
        aria-haspopup="true"
        onClick={handleClick}
        endIcon={<Person />}
      >
        {user.id}
      </Button>
      <Menu
        id="user-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem
          onClick={() => {
            setShowUpdatePasswordDialog(true)
            handleClose()
          }}
        >
          {t('update_password.change_password')}
        </MenuItem>
      </Menu>
    </Box>
  )
}
