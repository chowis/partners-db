import { format, parse } from 'date-fns'

export function parseDateString(date: string, fallback?: string) {
  try {
    let val = date
    if (!val) {
      return fallback
    }

    if (val.length > 8) {
      val = val.substring(0, 8)
    }

    return format(parse(val, 'yyyyMMdd', new Date()), 'yyyy-MM-dd')
  } catch (error) {
    return fallback
  }
}
